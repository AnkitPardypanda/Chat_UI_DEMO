package com.example.root.customelistviews;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ScrollView;

import java.util.ArrayList;

/**
 * Created by root on 13/12/16.
 */
public class DeepScrollView extends ScrollView {
    private static final int SWIPE_MIN_DISTANCE = 5;
    private static final int SWIPE_THRESHOLD_VELOCITY = 300;
    private OnScrollViewListener mOnScrollViewListener;
    private ArrayList mItems = null;
    private GestureDetector mGestureDetector;
    private int mActiveFeature = 0;
    private Runnable scrollerTask;
    private int initialPosition;
    private OnScrollStoppedListener onScrollStoppedListener;
    private int newCheck = 100;
    private static final String TAG = "MyScrollView";

    public DeepScrollView(Context context) {
        super(context);

    }

    public DeepScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        scrollerTask = new Runnable() {

            public void run() {

                int newPosition = getScrollY();
                if(initialPosition - newPosition == 0){//has stopped

                    if(onScrollStoppedListener!=null){
Log.d("new position",String.valueOf(newPosition));
                        onScrollStoppedListener.onScrollStopped();
                    }
                }else{
                    initialPosition = getScrollY();
                   postDelayed(scrollerTask, newCheck);
                }
            }
        };
    }

    public DeepScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void scrollToDeepChild(int viewId) {
        scrollToDeepChild(findViewById(viewId));
    }

    public void scrollToDeepChild(View child) {
        final Point childOffset = new Point();
        getDeepChildOffset(child.getParent(), child, childOffset);
        final Rect childRect = new Rect(childOffset.x, childOffset.y, childOffset.x + child.getWidth(), childOffset.y + child.getHeight());
        final int deltay = computeScrollDeltaToGetChildRectOnScreen(childRect);
        smoothScrollBy(0, deltay + getDeepChildTopMargin(child));
        requestLayout();
        invalidate();
    }

    private void getDeepChildOffset(ViewParent nextParent, View nextChild, Point accumulatedOffset) {
        final ViewGroup parent = (ViewGroup) nextParent;
        if (parent == this) {
            return;
        } accumulatedOffset.x += nextChild.getLeft();
        accumulatedOffset.y += nextChild.getTop();

        getDeepChildOffset(parent.getParent(), parent, accumulatedOffset);
    }

    private int getDeepChildTopMargin(View child) {
        int topMargin = 0;
        if (child.getLayoutParams() instanceof MarginLayoutParams) {
            topMargin = ((MarginLayoutParams) child.getLayoutParams()).topMargin;
        }
        return topMargin;
    }

    public void SpappingView() {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mGestureDetector.onTouchEvent(event)) {
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    int scrollX = getScrollX();
                    int featureWidth = v.getMeasuredWidth();
                    mActiveFeature = ((scrollX + (featureWidth / 2)) / featureWidth);
                    int scrollTo = mActiveFeature * featureWidth;
                    ///   smoothScrollTo(scrollTo, 0);
                    return true;
                } else {
                    return false;
                }
            }
        });
        mGestureDetector = new GestureDetector(new MyGestureDetector());
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        View view = (View) getChildAt(getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()-(getHeight()+getScrollY()));

        // if diff is zero, then the bottom has been reached
        if( diff == 0 )
        {
            // notify that we have reached the bottom


            if(onScrollStoppedListener!=null){
                view.getId();
                Log.d("", "MyScrollView: Bottom has been reached" );
                onScrollStoppedListener.onScrollStopped();
            }
        }

        super.onScrollChanged(l, t, oldl, oldt);

    }

    public interface OnScrollViewListener {
        void onScrollChanged(DeepScrollView v, int l, int t, int oldl, int oldt);
    }

    public void setOnScrollViewListener(OnScrollViewListener l) {
        this.mOnScrollViewListener = l;
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {






          /*  try {
                //right to left
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    int featureWidth = getMeasuredWidth();
                    mActiveFeature = (mActiveFeature < (mItems.size() - 1))? mActiveFeature + 1:mItems.size() -1;
                    smoothScrollTo(mActiveFeature*featureWidth, 0);
                    return true;
                }
                //left to right
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    int featureWidth = getMeasuredWidth();
                    mActiveFeature = (mActiveFeature > 0)? mActiveFeature - 1:0;
                    smoothScrollTo(mActiveFeature*featureWidth, 0);
                    return true;
                }
            } catch (Exception e) {
                Log.e("Fling", "There was an error processing the Fling event:" + e.getMessage());
            }
            */
            return false;
        }
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        super.setOnTouchListener(l);
    }

    public void setOnScrollStoppedListener(OnScrollStoppedListener listener){
        onScrollStoppedListener = listener;
    }

    public void startScrollerTask(){

        initialPosition = getScrollY();
       postDelayed(scrollerTask, newCheck);
    }

    public interface OnScrollStoppedListener{
        void onScrollStopped();
    }
}
