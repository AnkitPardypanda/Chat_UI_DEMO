package com.example.root.customelistviews;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 16/12/16.
 */
public class FullScreenChat extends Activity {

    private EditText mEditTextMessage;
    private TextView mTextViewSend;
    private ImageView mImageViewCloseWindow, mImageViewMessage;
    private TextView Textviewpostion;
    private ListView ListviewHashmapType;
    private ArrayList<HashMap<String, String>> mapList = new ArrayList<HashMap<String, String>>();
    private SimpleAdapter myAdapter;
    private LinearLayout mLinearLayoutTitle;
    private TextView mTextviewUserName;
    private String user_name;
    private String user_colors;
    private LinearLayout mLinearLayoutButtom;
    private ImageView mImageViewUsersPic;
    private String Boundal_user_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.chat_screen);
        mEditTextMessage = (EditText) findViewById(R.id.mEditTextMessage);
        mTextViewSend = (TextView) findViewById(R.id.TextViewSend);
        Textviewpostion = (TextView) findViewById(R.id.Textviewpostion);
        mImageViewUsersPic = (ImageView) findViewById(R.id.ImageViewUsersPic);
        ListviewHashmapType = (ListView) findViewById(R.id.ListviewHashmapType);
        mLinearLayoutTitle = (LinearLayout) findViewById(R.id.LinearLayoutTitle);
        mTextviewUserName = (TextView) findViewById(R.id.TextviewUserName);
        mImageViewCloseWindow = (ImageView) findViewById(R.id.ImageViewCloseWindow);
        mImageViewMessage = (ImageView) findViewById(R.id.ImageViewMessage);
        mLinearLayoutButtom = (LinearLayout) findViewById(R.id.LinearLayoutButtom);
        Bundle b = getIntent().getExtras();
        user_name = b.getString("user_id");
        user_colors = b.getString("user_bg_colors");
        Boundal_user_url = b.getString("user_url");
        mLinearLayoutButtom.setVisibility(View.GONE);
        for (int i = 0; i <= 10; i++) {
            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", String.valueOf(Fragment1.getRandomColor()));
            selectedItem1.put("position", "position  " + String.valueOf(i));
            selectedItem1.put("screen_pix", "position  " + String.valueOf(Classes.Screeen_size));
            mapList.add(selectedItem1);
        }
        mTextviewUserName.setText(user_name);
        Picasso.with(getApplicationContext())
                .load(Boundal_user_url)
                .resize(100, 100).transform(new CircleTransform())
                .centerCrop().placeholder(R.drawable.defult_users)
                .into(mImageViewUsersPic);
        if (Build.VERSION.SDK_INT >= 16) {
            Textviewpostion.setBackground(Main2Activity.drawCircle(getApplicationContext(), 50, 50, Integer.parseInt(user_colors), 0));
        } else {
            Textviewpostion.setBackgroundDrawable(Main2Activity.drawCircle(getApplicationContext(), 50, 50, Integer.parseInt(user_colors), 0));
        }
        myAdapter = new SimpleAdapter(this, mapList);
        Textviewpostion.setText(user_name);
        ListviewHashmapType.setAdapter(myAdapter);

        mTextViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> selectedItem1 = new HashMap<String, String>();
                selectedItem1.put("color_code", String.valueOf(Fragment1.getRandomColor()));
                selectedItem1.put("position", mEditTextMessage.getText().toString());
                mapList.add(selectedItem1);
                myAdapter.notifyDataSetChanged();
                ListviewHashmapType.setSelection(ListviewHashmapType.getCount() - 1);

            }
        });
        setMessageActive();
        mImageViewCloseWindow.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();

            }
        });
        Textviewpostion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkingView();
            }
        });
        mLinearLayoutTitle.setOnClickListener(new View.OnClickListener() {
            public int ip;

            @Override
            public void onClick(View v) {
                ip++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        ip = 0;
                    }
                };

                if (ip == 1) {
                    //Single click
                    checkingView();
                    handler.postDelayed(r, 250);
                } else if (ip == 2) {
                    //Double click
                    ip = 0;

                    finish();
                }

            }
        });
    }

    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;
        private ArrayList<HashMap<String, String>> mapList;

        public SimpleAdapter(Activity mActivity, ArrayList<HashMap<String, String>> mapList) {
            this.mActivity = mActivity;
            this.mapList = mapList;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override




        public int getCount() {
            return mapList.size();
        }

        @Override
        public Object getItem(int position) {
            return mapList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.message, null);
                holder = new ViewHolder();
                holder.mTextviewpostition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.contentWithBackground = (LinearLayout) convertView.findViewById(R.id.contentWithBackground);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = mapList.get(position);
            // holder.mView.setBackgroundColor(Integer.parseInt(selectedItem.get("color_code").toString()));
            holder.mTextviewpostition.setText(selectedItem.get("position").toString());
            ///////////////////size layout here....
            convertView.setOnClickListener(new View.OnClickListener() {
                public int ip;

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ip++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            ip = 0;
                        }
                    };

                    if (ip == 1) {
                        //
                        // Single click
                        handler.postDelayed(r, 250);
                    } else if (ip == 2) {
                        //
                        // Double click
                        ip = 0;

                        finish();
                    }


                }
            });
            return convertView;
        }
    }

    public void setMessageActive() {
        mImageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkingView();
            }
        });
    }

    public void checkingView() {
        if (mLinearLayoutButtom.getVisibility() == View.VISIBLE) {
            mEditTextMessage.setText("");
            mLinearLayoutButtom.setVisibility(View.GONE);
        } else {
            mEditTextMessage.setText("");
            mLinearLayoutButtom.setVisibility(View.VISIBLE);
        }
    }

    public class ViewHolder {

        TextView mTextviewpostition;
        LinearLayout contentWithBackground;
    }
}
