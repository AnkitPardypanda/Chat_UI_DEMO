package com.example.root.customelistviews;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 5/1/17.
 */
public class Classes {


    public static ArrayList<String> add_value = new ArrayList<>();
    public static ArrayList<String> put_user_name, put_user_pic;
    public static ArrayList<HashMap<String, String>> UserPositionInfomrmation = new ArrayList<HashMap<String, String>>();
    public static int Screeen_size = 0;
    public static int screen_heights = 150;

    public static int getProgamaticalHeights(boolean mCondition, Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (mCondition == true) {
            int width = size.x;
            return width;
        } else {
            int height = size.y;
            return height;
        }
    }


    public static void setArrayFirstTime() {
        add_value.clear();
        for (int i = 0; i <= 10; i++) {
            add_value.add(String.valueOf(i));
        }
        Log.d("array value", add_value.toString());

        //////////////////Hashmap type
    /*    for (int i = 0; i <= 10; i++) {

            HashMap<String,String> user_object=new HashMap<>();
            user_object.put("user_background","0");
            Classes.addUserInformation(user_object);
        }
        getUserInformationLog();*/
    }

    public static void setTopPossition(int TopPosition) {
        add_value.remove(TopPosition);
        Log.d("array Tremove", add_value.toString());
        //////////////////Hashmap type remove
      /*  removeUserPositionInfomrmation(TopPosition);
        getUserInformationLog();*/
    }

    public static void addToTop(String value) {
        add_value.add(0, value);
        Log.d("array addTop", add_value.toString());
///////////hash map add to top
/*
        HashMap<String,String> user_object=new HashMap<>();
        user_object.put("user_background","0000000");
        UserPositionInfomrmation.add(0,user_object);
        getUserInformationLog();

        */
    }

    public static void AddButttom(int Buttomposition, String value) {

        add_value.remove(Buttomposition);
        Log.d("array Bremove", add_value.toString());
        add_value.add(value);
        Log.d("array addButtom", add_value.toString());
        ///////////////////hash map add buttom
         /*
        UserPositionInfomrmation.remove(Buttomposition);
        getUserInformationLog();
        HashMap<String,String> user_object=new HashMap<>();
        user_object.put("user_background","111111");
        UserPositionInfomrmation.add(user_object);
        getUserInformationLog();
        */
    }


    ///////////////////////////////////////////    Implement
    public static void setClearUserPositionInfomrmation() {
        if (!UserPositionInfomrmation.isEmpty()) {
            UserPositionInfomrmation.clear();
        }
    }

    public static void addUserInformation(HashMap<String, String> mObject) {
        UserPositionInfomrmation.add(mObject);
    }

    public static void getUserInformationLog() {
        //// Log.d("User info", UserPositionInfomrmation.toString());
    }

    public static void updateUserPositionInfomrmation(int position, HashMap<String, String> mObject) {
        UserPositionInfomrmation.set(position, mObject);
    }

    public static void removeUserPositionInfomrmation(int position) {
        UserPositionInfomrmation.remove(position);
    }

    public static HashMap<String, String> getUserInfoByPosition(int position) {

        Log.d("user info", String.valueOf(UserPositionInfomrmation.get(position)));
        return UserPositionInfomrmation.get(position);


    }

    public static void addUserName() {
        put_user_name = new ArrayList<>();
        put_user_name.add("Captain Jack Sparrow");
        put_user_name.add("Will Turner");
        put_user_name.add("Elizabeth Swann");
        put_user_name.add("Abbey");
        put_user_name.add("Thomas");
        put_user_name.add("Nicholas");
        put_user_name.add("Jason");
        put_user_name.add("Sagalevitch");
        put_user_name.add("Cutler Beckett");
        put_user_name.add("Suzy");
        put_user_name.add("Warner");
        put_user_name.add("Garber");
        put_user_name.add("Hector Barbossa");
        put_user_name.add("Amis");
        put_user_name.add("Bruce");


    }

    public static void addUserPic() {
        put_user_pic = new ArrayList<>();

        put_user_pic.add("http://wallpapercave.com/wp/IRkkT7K.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/song_of_the_sea_cartoon_saoirse_104496_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/redhead_girl_laugh_nice_funny_humor_55186_200x300.jpg");
        put_user_pic.add("https://i0.wallpaperscraft.com/image/cartoon_minions_2015_103247_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/hattie_watson_freckles_eyes_girl_red_31138_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/jackie_chan_actor_white_suit_smile_98797_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/dwayne_johnson_actor_bald_hair_celebrity_98864_200x300.jpg");
        put_user_pic.add("https://i0.wallpaperscraft.com/image/arya_stark_game_of_thrones_maisie_williams_102384_200x300.jpg");
        put_user_pic.add("https://i0.wallpaperscraft.com/image/mortal_kombat_x_scorpio_ninja_pose_96758_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/battle_of_the_immortals_warrior_art_fire_weapons_armor_100698_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/santa_claus_pirate_ship_gifts_sea_storm_57522_200x300.jpg");
        put_user_pic.add("https://i2.wallpaperscraft.com/image/new_year_christmas_cat_card_98535_200x300.jpg");
        put_user_pic.add("https://i1.wallpaperscraft.com/image/redhead_girl_laugh_nice_funny_humor_55186_200x300.jpg");
        put_user_pic.add("http://www.techagesite.com/zelda/link-zelda-wallpaper-sword-4991x2810.jpg");
        put_user_pic.add("http://www.techagesite.com/zelda/link-zelda-wallpaper-sword-sheild-figurine-wallpaper-1500x1000.JPG");
    }

    public static String getUserName(int Position) {
        return put_user_name.get(Position);
    }

    public static String getUserUrl(int Position) {
        return put_user_pic.get(Position);
    }


    public static void showSoftKeyboard(View view, Activity mact) {
        InputMethodManager inputMethodManager = (InputMethodManager) mact.getSystemService(mact.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    public static void hideSoftKeyboard(Activity mact) {
        if (mact.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) mact.getSystemService(mact.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mact.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void setMineHandler(long times, final handlerInterface minterFace) {
        Handler my_handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                minterFace.run();
            }
        };
        my_handler.postDelayed(r, times);
    }

    interface handlerInterface {
        void run();
    }

    public static void reMoveScrolviedItems(LinearLayout linearLayoutFragments, int removewViewID) {
        LinearLayout et = (LinearLayout) linearLayoutFragments.findViewById(removewViewID);
        linearLayoutFragments.removeView(et);
    }

    public static void addViewDynamic(final LinearLayout linearLayoutFragments, Activity mActivity, int removewViewID) {
        final LinearLayout ll = new LinearLayout(mActivity);
        ll.setId(removewViewID);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        TextView product = new TextView(mActivity);
        product.setText("ankiu  ");

        ll.addView(product);
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                linearLayoutFragments.addView(ll);
            }
        });
    }

    public static void sendBroadCast(Activity activity, String Actions, String value) {

        Intent intentUpdate = new Intent();
        intentUpdate.setAction(Actions);
        intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
        intentUpdate.putExtra("value", value);
        activity.sendBroadcast(intentUpdate);
    }

    public static void initBroadCast(BroadcastReceiver mBroadcastReceiver, Activity activity, String Actions) {
        IntentFilter intentFilter_update = new IntentFilter(Actions);
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        activity.registerReceiver(mBroadcastReceiver, intentFilter_update);
    }
}
