package com.example.root.customelistviews;

/**
 * Created by root on 6/12/16.
 */

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Fragment1Demos extends Fragment {
    private EditText mEditTextMessage;
    private TextView mTextViewSend;
    private int status_bar_height;
    private ListView ListviewHashmapType;
    private ArrayList<HashMap<String, String>> mapList = new ArrayList<HashMap<String, String>>();
    private SimpleAdapter myAdapter;
    private LinearLayout mLinearLayoutSize;
    private TextView Textviewpostion;
    private float pixels;
    private ImageView mImageViewUsersPic;
    private CommonBroadCast mCommonBroadCast;
    private String Boundal_value, Boundal_user_url;
    private LinearLayout mLinearLayoutButtom;
    private LinearLayout mLinearLayoutTitle;
    private String position_view;
    private ImageView mImageViewCloseWindow, mImageViewMessage;
    private long mLastClickTime = 0;
    private TextView mTextviewUserName;
    private String user_bg_colors;
    private ResizedBroadcast mResizedBroadcast;
    private int gloable_reduce_height = 0;
    private ImageView mImageViewService_2;
    private int gloableChangeHeight = 0;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        ///   name = getArguments().getString("key");
        View rootView = inflater.inflate(R.layout.chat_screen, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mEditTextMessage = (EditText) rootView.findViewById(R.id.mEditTextMessage);
        mTextViewSend = (TextView) rootView.findViewById(R.id.TextViewSend);
        mImageViewService_2 = (ImageView) rootView.findViewById(R.id.mImageViewService_2);
        Textviewpostion = (TextView) rootView.findViewById(R.id.Textviewpostion);
        mImageViewUsersPic = (ImageView) rootView.findViewById(R.id.ImageViewUsersPic);
        ListviewHashmapType = (ListView) rootView.findViewById(R.id.ListviewHashmapType);
        mLinearLayoutSize = (LinearLayout) rootView.findViewById(R.id.LinearLayoutSize);
        mLinearLayoutButtom = (LinearLayout) rootView.findViewById(R.id.LinearLayoutButtom);
        mLinearLayoutTitle = (LinearLayout) rootView.findViewById(R.id.LinearLayoutTitle);
        mImageViewCloseWindow = (ImageView) rootView.findViewById(R.id.ImageViewCloseWindow);
        mImageViewMessage = (ImageView) rootView.findViewById(R.id.ImageViewMessage);
        mTextviewUserName = (TextView) rootView.findViewById(R.id.TextviewUserName);
        Bundle b = getArguments();
        position_view = b.getString("position");
        Boundal_user_url = b.getString("user_url");
        Boundal_value = String.valueOf(position_view);
        Log.d("fragment", String.valueOf(Boundal_user_url));
        mLinearLayoutButtom.setVisibility(View.GONE);
        Textviewpostion.setText(Boundal_value);
        mTextviewUserName.setText(Boundal_value);
        user_bg_colors = b.getString("colors");
        mEditTextMessage.setFocusableInTouchMode(true);
        mEditTextMessage.setFocusable(true);

        Picasso.with(getActivity().getApplicationContext())
                .load(Boundal_user_url)
                .resize(100, 100).transform(new CircleTransform())
                .centerCrop().placeholder(R.drawable.defult_users)
                .into(mImageViewUsersPic);
        if (Build.VERSION.SDK_INT >= 16) {
            Textviewpostion.setBackground(Main2Activity.drawCircle(getActivity(), 50, 50, Integer.parseInt(b.getString("colors")), 0));
        } else {
            Textviewpostion.setBackgroundDrawable(Main2Activity.drawCircle(getActivity(), 50, 50, Integer.parseInt(b.getString("colors")), 0));
        }


        setChatWindowHeight();

        /// setChatWindowHeight();
        setChatOpen();

        setMessageActive();


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        gloable_reduce_height = 150 + getStatusBarHeight(getActivity());
        if (Classes.Screeen_size == 0) {
            mLinearLayoutSize.getLayoutParams().height = height - gloable_reduce_height;
        } else if ((Classes.Screeen_size == 1)) {
            int result = height - gloable_reduce_height;
            mLinearLayoutSize.getLayoutParams().height = result / 2;
        } else {
            int result = height - gloable_reduce_height;
            mLinearLayoutSize.getLayoutParams().height = result / 3;
        }


        for (int i = 0; i <= 10; i++) {
            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", String.valueOf(getRandomColor()));
            selectedItem1.put("position", "position  " + String.valueOf(i));
            selectedItem1.put("screen_pix", "position  " + String.valueOf(Classes.Screeen_size));
            mapList.add(selectedItem1);
        }

        myAdapter = new SimpleAdapter(getActivity(), mapList);

        ListviewHashmapType.setAdapter(myAdapter);

        mTextViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditTextMessage.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Message blank", Toast.LENGTH_LONG).show();
                } else {
                    HashMap<String, String> selectedItem1 = new HashMap<String, String>();
                    selectedItem1.put("color_code", String.valueOf(getRandomColor()));
                    selectedItem1.put("position", mEditTextMessage.getText().toString());
                    mEditTextMessage.setText("");
                    mapList.add(selectedItem1);
                    myAdapter.notifyDataSetChanged();
                    ListviewHashmapType.setSelection(ListviewHashmapType.getCount() - 1);
                }

            }
        });

        ListviewHashmapType.setOnTouchListener(new ListView.OnTouchListener() {
            public int ip;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();

                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        ip++;
                        Handler handler = new Handler();
                        Runnable r = new Runnable() {

                            @Override
                            public void run() {
                                ip = 0;
                            }
                        };

                        if (ip == 1) {
                            //Single click
                            checkingView();
                            handler.postDelayed(r, 250);


                        } else if (ip == 2) {
                            //Double click
                            ip = 0;

                            Intent inten = new Intent(getActivity(), FullScreenChat.class);
                            inten.putExtra("user_id", Boundal_value);
                            inten.putExtra("user_bg_colors", user_bg_colors);
                            inten.putExtra("user_url", Boundal_user_url);
                            startActivity(inten);
                        }

                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;


                }

                // Handle ListView touch events.
                v.onTouchEvent(event);


                return true;
            }
        });


        Textviewpostion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Intent intentUpdate = new Intent();
                intentUpdate.setAction("action_key");
                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                intentUpdate.putExtra("extract_key", String.valueOf(position_view));
                getActivity().sendBroadcast(intentUpdate);*/

                checkingView();
            }
        });

        mImageViewCloseWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(getActivity(), FullScreenChat.class);
                inten.putExtra("user_id", Boundal_value);
                inten.putExtra("user_bg_colors", user_bg_colors);
                inten.putExtra("user_url", Boundal_user_url);
                startActivity(inten);
            }
        });


        mLinearLayoutTitle.setOnClickListener(new View.OnClickListener() {

            public int ip;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                ip++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        ip = 0;

                    }
                };

                if (ip == 1) {
                    //Single click
                    checkingView();
                    handler.postDelayed(r, 250);

                } else if (ip == 2) {
                    //Double click

                    ip = 0;
                    Toast.makeText(getActivity(), "2", Toast.LENGTH_LONG).show();
                    Intent inten = new Intent(getActivity(), FullScreenChat.class);
                    inten.putExtra("user_id", Boundal_value);
                    inten.putExtra("user_bg_colors", user_bg_colors);
                    inten.putExtra("user_url", Boundal_user_url);
                    startActivity(inten);
                }


            }
        });

        return rootView;

    }

//


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(getActivity(), "keyboard visible", Toast.LENGTH_SHORT).show();
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(getActivity(), "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }

    public static int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;
        private ArrayList<HashMap<String, String>> mapList;

        public SimpleAdapter(Activity mActivity, ArrayList<HashMap<String, String>> mapList) {
            this.mActivity = mActivity;
            this.mapList = mapList;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return mapList.size();
        }

        @Override
        public Object getItem(int position) {
            return mapList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.message, null);
                holder = new ViewHolder();
                holder.mTextviewpostition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.contentWithBackground = (LinearLayout) convertView.findViewById(R.id.contentWithBackground);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = mapList.get(position);
            // holder.mView.setBackgroundColor(Integer.parseInt(selectedItem.get("color_code").toString()));
            holder.mTextviewpostition.setText(selectedItem.get("position").toString());
///////////////////size layout here....
         /*   holder.contentWithBackground.setOnClickListener(new View.OnClickListener() {

                public int ip;

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ip++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            ip = 0;
                        }
                    };

                    if (ip == 1) {
                        //Single click
                        handler.postDelayed(r, 250);
                    } else if (ip == 2) {
                        //Double click
                        ip = 0;

                        Intent inten = new Intent(getActivity(), FullScreenChat.class);
                        inten.putExtra("user_id",Boundal_value);
                        inten.putExtra("user_bg_colors",user_bg_colors);
                        startActivity(inten);
                    }


                }
            });*/


            return convertView;
        }
    }

    public class ViewHolder {

        TextView mTextviewpostition;
        LinearLayout contentWithBackground;
    }

    class CommonBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String update = intent.getStringExtra("extract_key");
            if (mLinearLayoutButtom.getVisibility() == View.VISIBLE) {
                mLinearLayoutButtom.setVisibility(View.GONE);
            }

        }
    }


    class ResizedBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            int gloable_heights = intent.getIntExtra("resize_height", 0);
            if (gloable_heights == 0) {
                mLinearLayoutSize.getLayoutParams().height = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
            } else if ((gloable_heights == 1)) {
                int result = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 2;
            } else {
                int result = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 3;
            }

        }
    }

    public void setChatWindowHeight() {
        mResizedBroadcast = new ResizedBroadcast();
        IntentFilter intentFilter_update = new IntentFilter("action_resize");
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(mResizedBroadcast, intentFilter_update);
    }

    public void setMessageActive() {
        mImageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                checkingView();
            }
        });
    }

    public void checkingView() {
        if (mLinearLayoutButtom.getVisibility() == View.VISIBLE) {
            mEditTextMessage.setText("");
            mLinearLayoutButtom.setVisibility(View.GONE);


            ///// Defulte size


            if (Classes.Screeen_size == 0) {
                mLinearLayoutSize.getLayoutParams().height = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
            } else if ((Classes.Screeen_size == 1)) {
                int result = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 2;
            } else {
                int result = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 3;
            }


        } else {





            mEditTextMessage.setText("");
            mLinearLayoutButtom.setVisibility(View.VISIBLE);
            mEditTextMessage.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().getApplicationContext().INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

            /////Custom heights
            int perfect_height = 0;
            if (Classes.Screeen_size == 0) {
                perfect_height = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = perfect_height + 100;
            } else if ((Classes.Screeen_size == 1)) {
                int result = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                perfect_height = result / 2;
                mLinearLayoutSize.getLayoutParams().height = perfect_height + 100;
            } else {
                int result = Classes.getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                perfect_height = result / 3;
                mLinearLayoutSize.getLayoutParams().height = perfect_height + 100;
            }


        }

    }

    public void setChatOpen() {
        mCommonBroadCast = new CommonBroadCast();
        IntentFilter intentFilter_update = new IntentFilter("action_key");
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(mCommonBroadCast, intentFilter_update);
    }

    public static int getProgamaticalHeights(boolean mCondition, Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (mCondition == true) {
            int width = size.x;
            return width;
        } else {
            int height = size.y;
            return height;
        }
    }

    public static int getStatusBarHeight(Activity mActivity) {
        int result = 0;
        int resourceId = mActivity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mActivity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


}
