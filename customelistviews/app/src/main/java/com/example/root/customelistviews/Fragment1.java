package com.example.root.customelistviews;

/**
 * Created by root on 6/12/16.
 */

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import junit.framework.Test;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Fragment1 extends Fragment {
    private ChatEditText mEditTextMessage;
    private TextView mTextViewSend;
    private int status_bar_height;
    private ListView ListviewHashmapType;
    private ArrayList<HashMap<String, String>> mapList = new ArrayList<HashMap<String, String>>();
    private SimpleAdapter myAdapter;
    private LinearLayout mLinearLayoutSize;
    private TextView Textviewpostion;
    private float pixels;
    private ImageView mImageViewUsersPic;

    private String Boundal_value, Boundal_user_url;
    private LinearLayout mLinearLayoutButtom;
    private LinearLayout mLinearLayoutTitle;
    private String position_view;
    private ImageView mImageViewCloseWindow, mImageViewMessage;
    private long mLastClickTime = 0;
    private TextView mTextviewUserName;
    private String user_bg_colors;
    private ResizedBroadcast mResizedBroadcast;
    private int gloable_reduce_height = 0;
    private ImageView mImageViewService_2;
    private int gloableChangeHeight = 0;
    private String fragment_id;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        ///   name = getArguments().getString("key");
        View rootView = inflater.inflate(R.layout.chat_screen, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mEditTextMessage = (ChatEditText) rootView.findViewById(R.id.mEditTextMessage);
        mTextViewSend = (TextView) rootView.findViewById(R.id.TextViewSend);
        mImageViewService_2 = (ImageView) rootView.findViewById(R.id.mImageViewService_2);
        Textviewpostion = (TextView) rootView.findViewById(R.id.Textviewpostion);
        mImageViewUsersPic = (ImageView) rootView.findViewById(R.id.ImageViewUsersPic);
        ListviewHashmapType = (ListView) rootView.findViewById(R.id.ListviewHashmapType);
        mLinearLayoutSize = (LinearLayout) rootView.findViewById(R.id.LinearLayoutSize);
        mLinearLayoutButtom = (LinearLayout) rootView.findViewById(R.id.LinearLayoutButtom);
        mLinearLayoutTitle = (LinearLayout) rootView.findViewById(R.id.LinearLayoutTitle);
        mImageViewCloseWindow = (ImageView) rootView.findViewById(R.id.ImageViewCloseWindow);
        mImageViewMessage = (ImageView) rootView.findViewById(R.id.ImageViewMessage);
        mTextviewUserName = (TextView) rootView.findViewById(R.id.TextviewUserName);
        Bundle b = getArguments();
        position_view = b.getString("position");
        Boundal_user_url = b.getString("user_url");
        fragment_id = b.getString("fragment_id");
        Boundal_value = String.valueOf(position_view);
        Log.d("fragment", String.valueOf(Boundal_user_url));
        mLinearLayoutButtom.setVisibility(View.GONE);
        Textviewpostion.setText(Boundal_value);
        mTextviewUserName.setText(Boundal_value);
        user_bg_colors = b.getString("colors");
        mEditTextMessage.setFocusableInTouchMode(true);
        mEditTextMessage.setFocusable(true);

        Picasso.with(getActivity().getApplicationContext())
                .load(Boundal_user_url)
                .resize(100, 100).transform(new CircleTransform())
                .centerCrop().placeholder(R.drawable.defult_users)
                .into(mImageViewUsersPic);
        if (Build.VERSION.SDK_INT >= 16) {
            Textviewpostion.setBackground(Main2Activity.drawCircle(getActivity(), 50, 50, Integer.parseInt(b.getString("colors")), 0));
        } else {
            Textviewpostion.setBackgroundDrawable(Main2Activity.drawCircle(getActivity(), 50, 50, Integer.parseInt(b.getString("colors")), 0));
        }


        setChatWindowHeight();

        /// setChatWindowHeight();
        Classes.initBroadCast(new HideEditTextCommonBroadCast(), getActivity(), "editText_hide_action");

        setMessageActive();
        mEditTextMessage.setKeyImeChangeListener(new ChatEditText.KeyImeChange() {
            @Override
            public void onKeyIme(int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_BACK == event.getKeyCode()) {
                    // do something

                    checkingView();
                }
            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        gloable_reduce_height = 150 + getStatusBarHeight(getActivity());
        if (Classes.Screeen_size == 0) {
            mLinearLayoutSize.getLayoutParams().height = height - gloable_reduce_height;
        } else if ((Classes.Screeen_size == 1)) {
            int result = height - gloable_reduce_height;
            mLinearLayoutSize.getLayoutParams().height = result / 2;
        } else {
            int result = height - gloable_reduce_height;
            mLinearLayoutSize.getLayoutParams().height = result / 3;
        }


        for (int i = 0; i <= 10; i++) {
            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", String.valueOf(getRandomColor()));
            selectedItem1.put("position", "position  " + String.valueOf(i));
            selectedItem1.put("screen_pix", "position  " + String.valueOf(Classes.Screeen_size));
            mapList.add(selectedItem1);
        }

        myAdapter = new SimpleAdapter(getActivity(), mapList);

        ListviewHashmapType.setAdapter(myAdapter);

        mTextViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditTextMessage.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Message blank", Toast.LENGTH_LONG).show();

                } else {
                    HashMap<String, String> selectedItem1 = new HashMap<String, String>();
                    selectedItem1.put("color_code", String.valueOf(getRandomColor()));
                    selectedItem1.put("position", mEditTextMessage.getText().toString());
                    mEditTextMessage.setText("");
                    mapList.add(selectedItem1);
                    myAdapter.notifyDataSetChanged();
                    ListviewHashmapType.setSelection(ListviewHashmapType.getCount() - 1);

                }

            }
        });

        ListviewHashmapType.setOnTouchListener(new ListView.OnTouchListener() {


            public int ip;
            public int singlaDoubleTapListner_value = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();

                switch (action) {
                    case MotionEvent.ACTION_DOWN:


                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ip++;
                                        Handler handler = new Handler();
                                        Handler my_handler = new Handler();
                                        Runnable SingleRunnable = null;
                                        Runnable r = new Runnable() {

                                            @Override
                                            public void run() {
                                                ip = 0;
                                            }
                                        };

                                        if (ip == 1) {
                                            //Single click
                                            ////checkingView();
                                            singlaDoubleTapListner_value = 0;

                                            SingleRunnable = new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (singlaDoubleTapListner_value == 0) {
                                                        checkingView();
                                                    }

                                                }
                                            };
                                            my_handler.postDelayed(SingleRunnable, 300);


                                            handler.postDelayed(r, 250);


                                        } else if (ip == 2) {
                                            //Double click
                                            singlaDoubleTapListner_value = 1;
                                            ip = 0;
                                            my_handler.removeCallbacks(SingleRunnable);
                                            Intent inten = new Intent(getActivity(), FullScreenChat.class);
                                            inten.putExtra("user_id", Boundal_value);
                                            inten.putExtra("user_bg_colors", user_bg_colors);
                                            inten.putExtra("user_url", Boundal_user_url);
                                            startActivity(inten);
                                        }
                                    }
                                });
                                return null;
                            }
                        }.execute();

                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;


                }

                // Handle ListView touch events.
                v.onTouchEvent(event);


                return true;
            }
        });


        Textviewpostion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkingView();
            }
        });

        mImageViewCloseWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(getActivity(), FullScreenChat.class);
                inten.putExtra("user_id", Boundal_value);
                inten.putExtra("user_bg_colors", user_bg_colors);
                inten.putExtra("user_url", Boundal_user_url);
                startActivity(inten);
            }
        });


        mLinearLayoutTitle.setOnClickListener(new View.OnClickListener() {

            public int ip;
            public int singlaDoubleTapListner_value = 0;
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ip++;
                                Handler handler = new Handler();
                                Handler my_handler = new Handler();
                                Runnable SingleRunnable = null;
                                Runnable r = new Runnable() {

                                    @Override
                                    public void run() {
                                        ip = 0;
                                    }
                                };

                                if (ip == 1) {
                                    //Single click
                                    ////checkingView();
                                    singlaDoubleTapListner_value = 0;

                                    SingleRunnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            if (singlaDoubleTapListner_value == 0) {
                                                checkingView();
                                            }

                                        }
                                    };
                                    my_handler.postDelayed(SingleRunnable, 300);


                                    handler.postDelayed(r, 250);


                                } else if (ip == 2) {
                                    //Double click
                                    singlaDoubleTapListner_value = 1;
                                    ip = 0;
                                    my_handler.removeCallbacks(SingleRunnable);
                                    Intent inten = new Intent(getActivity(), FullScreenChat.class);
                                    inten.putExtra("user_id", Boundal_value);
                                    inten.putExtra("user_bg_colors", user_bg_colors);
                                    inten.putExtra("user_url", Boundal_user_url);
                                    startActivity(inten);
                                }
                            }
                        });
                        return null;
                    }
                }.execute();


            }
        });

        return rootView;

    }

//


    public static int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;
        private ArrayList<HashMap<String, String>> mapList;

        public SimpleAdapter(Activity mActivity, ArrayList<HashMap<String, String>> mapList) {
            this.mActivity = mActivity;
            this.mapList = mapList;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return mapList.size();
        }

        @Override
        public Object getItem(int position) {
            return mapList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.message, null);
                holder = new ViewHolder();
                holder.mTextviewpostition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.contentWithBackground = (LinearLayout) convertView.findViewById(R.id.contentWithBackground);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = mapList.get(position);

            holder.mTextviewpostition.setText(selectedItem.get("position").toString());


            return convertView;
        }
    }

    public class ViewHolder {

        TextView mTextviewpostition;
        LinearLayout contentWithBackground;
    }

    class HideEditTextCommonBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String value = intent.getStringExtra("value");
            if (mLinearLayoutButtom.getVisibility() == View.VISIBLE) {
                mLinearLayoutButtom.setVisibility(View.GONE);
                mEditTextMessage.setText("");
                Classes.hideSoftKeyboard(getActivity());
                Toast.makeText(getActivity(), value, Toast.LENGTH_LONG).show();
                if (value != null) {
                    Classes.sendBroadCast(getActivity(), "update_ui_action", value);
                }
            }

        }
    }


    class ResizedBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            int gloable_heights = intent.getIntExtra("resize_height", 0);
            if (gloable_heights == 0) {
                mLinearLayoutSize.getLayoutParams().height = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
            } else if ((gloable_heights == 1)) {
                int result = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 2;
            } else {
                int result = getProgamaticalHeights(false, getActivity()) - gloable_reduce_height;
                mLinearLayoutSize.getLayoutParams().height = result / 3;
            }

        }
    }

    public void setChatWindowHeight() {
        mResizedBroadcast = new ResizedBroadcast();
        IntentFilter intentFilter_update = new IntentFilter("action_resize");
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(mResizedBroadcast, intentFilter_update);
    }

    public void setMessageActive() {
        mImageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                checkingView();
            }
        });
    }

    public void checkingView() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mLinearLayoutButtom.getVisibility() == View.VISIBLE) {
                            Classes.sendBroadCast(getActivity(), "childShowButtom", "2");
                            mEditTextMessage.setText("");
                            mLinearLayoutButtom.setVisibility(View.GONE);

                            Classes.hideSoftKeyboard(getActivity());


                            Classes.sendBroadCast(getActivity(), "update_ui_action", fragment_id);

                        } else {
                            Classes.sendBroadCast(getActivity(), "childShowButtom", "1");
                            ///  Classes.sendBroadCast(getActivity(),"initBroadCast","1");
                            mEditTextMessage.setText("");
                            mLinearLayoutButtom.setVisibility(View.VISIBLE);
                            mEditTextMessage.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().getApplicationContext().INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


                        }
                    }
                });
                //Do things...
                return null;
            }
        }.execute();


    }


    public static int getProgamaticalHeights(boolean mCondition, Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (mCondition == true) {
            int width = size.x;
            return width;
        } else {
            int height = size.y;
            return height;
        }
    }

    public static int getStatusBarHeight(Activity mActivity) {
        int result = 0;
        int resourceId = mActivity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mActivity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


}
