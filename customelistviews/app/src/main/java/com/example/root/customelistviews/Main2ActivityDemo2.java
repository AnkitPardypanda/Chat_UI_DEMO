package com.example.root.customelistviews;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Main2ActivityDemo2 extends FragmentActivity {

    private int auto_selected_panal = 0;
    LinearLayout linearLayoutFragments;
    private ListView mListviewUsers;
    private LinearLayout mLinearLayoutHeader;
    private TextView TextviewOne;
    private TextView TextviewTwo;
    private TextView TextviewThree;
    private ArrayList<Fragment> fragList;
    private DeepScrollView ScrollViewPosition;
    public static int size = 0;
    private Activity mActivity;
    private FrameLayout frameLayout;
    private ProgressDialog progress = null;

    private ArrayList<Integer> hold_viewID = new ArrayList<>();
    private ArrayList<Integer> hold_viewHeight = new ArrayList<>();
    private ArrayList<Integer> hold_TempviewHeight = new ArrayList<>();
    private ArrayList<String> gloable_array;
    private SimpleAdapter adapter;
    ArrayList<Integer> gloable_exiting_view_id = new ArrayList<>();
    private UpdateBroadCast mUpdateBroadCast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mActivity = this;
        mListviewUsers = (ListView) findViewById(R.id.ListviewUsers);
        mLinearLayoutHeader = (LinearLayout) findViewById(R.id.LinearLayoutHeader);
        ScrollViewPosition = (DeepScrollView) findViewById(R.id.ScrollViewPosition);
        TextviewOne = (TextView) findViewById(R.id.TextviewOne);
        TextviewTwo = (TextView) findViewById(R.id.TextviewTwo);
        TextviewThree = (TextView) findViewById(R.id.TextviewThree);
        fragList = new ArrayList<Fragment>();
      ////  mListviewUsers.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        mLinearLayoutHeader.getLayoutParams().height = 150;
        creeateView(1, 1);

        updateListViewBackgroundColors();
        setIntentUpdateBroadCast();




        ScrollViewPosition.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ScrollViewPosition.startScrollerTask();
                }
                return false;
            }
        });
        ScrollViewPosition.setOnScrollStoppedListener(new DeepScrollView.OnScrollStoppedListener() {


            @Override
            public void onScrollStopped() {
                updateView();

            }
        });
        TextviewOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 0;
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.show();
                fragList.clear();
                Classes.Screeen_size = 0;
                creeateView(0, 1);
            }
        });
        TextviewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 1;
                Classes.Screeen_size = 1;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                creeateView(0, 2);
            }
        });


        TextviewThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                auto_selected_panal = 2;
                Classes.Screeen_size = 2;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                creeateView(0, 3);
               /* Classes.setArrayFirstTime();
                Classes.setTopPossition(3);
                Classes.addToTop("3");
                Classes.AddButttom(0, "3");*/

            }
        });


/*

         sortList = new ArrayList<HashMap<String, String>>();
        for (int ii = 1; ii <= 16; ii++) {
            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", String.valueOf(getRandomColor()));
            selectedItem1.put("position", String.valueOf(ii));
            selectedItem1.put("bg", "0");
            sortList.ad d(selectedItem1);
        }
        SimpleAdapter adapter = new SimpleAdapter(this, sortList);
        mListviewUsers.setAdapter(adapter);


        */


    }
public void updateView(){
    int selected_position = 0;

    if (auto_selected_panal == 0) {
        /////    Toast.makeText(mActivity, "single chat", Toast.LENGTH_LONG).show();

        if (!hold_viewHeight.isEmpty()) {
            hold_viewHeight.clear();
            hold_viewID.clear();
            hold_TempviewHeight.clear();
        }
        try {
            for (int i = 1; i < Classes.put_user_name.size(); i++) {
                Object viewID = i;

                View convertView = findViewById(i);
                Log.d("id", convertView.toString());
                if (isViewVisbleActual(convertView, i)) {
                    Log.d("visble", "yes");

                } else {
                    Log.d("visble", "no");
                }
            }
        } finally {
            if (hold_viewHeight.size() > 0) {
                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));

                Integer i = Collections.max(hold_viewHeight);
                Log.d("height", String.valueOf(i));
                int getViewID = 0;
                for (int position = 0; position <= hold_viewHeight.size(); position++) {
                    if (i == hold_viewHeight.get(position)) {
                        getViewID = hold_viewID.get(position);
                        break;
                    }
                }
                selected_position = getViewID;
                ScrollViewPosition.scrollToDeepChild(getViewID);
            }
        }
    } else if (auto_selected_panal == 1) {
        /////  Toast.makeText(mActivity, "Double chat", Toast.LENGTH_LONG).show();

        if (!hold_viewHeight.isEmpty()) {
            hold_viewHeight.clear();
            hold_viewID.clear();
            hold_TempviewHeight.clear();
        }
        try {
            for (int i = 1; i < Classes.put_user_name.size(); i++) {
                Object viewID = i;

                View convertView = findViewById(i);
                Log.d("id", convertView.toString());
                if (isViewVisbleActual(convertView, i)) {
                    Log.d("visble", "yes");
                } else {
                    Log.d("visble", "no");
                }
            }
        } finally {
            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
            Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
            Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
            Collections.sort(hold_viewHeight);
            Log.d("old array acc order", hold_viewHeight.toString());
            Log.d("temp array a order", hold_TempviewHeight.toString());

            if (hold_viewHeight.size() == 1) {

            } else {
                if (hold_viewHeight.size() > 0) {
                    int getViewID = 0;
                    Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 2)));
                    int heights = hold_viewHeight.get(hold_viewHeight.size() - 2);
                    Log.d("heights", String.valueOf(heights));
                    for (int position = 0; position < hold_TempviewHeight.size(); position++) {
                        if (heights == hold_TempviewHeight.get(position)) {
                            getViewID = hold_viewID.get(position);
                            break;
                        }
                    }
                    selected_position = getViewID;
                    ScrollViewPosition.scrollToDeepChild(getViewID);
                }
            }
        }


    } else if (auto_selected_panal == 2) {
        if (!hold_viewHeight.isEmpty()) {
            hold_viewHeight.clear();
            hold_viewID.clear();
            hold_TempviewHeight.clear();
        }
        try {
            for (int i = 1; i < Classes.put_user_name.size(); i++) {
                Object viewID = i;

                View convertView = findViewById(i);
                Log.d("id", convertView.toString());
                if (isViewVisbleActual(convertView, i)) {
                    Log.d("visble", "yes");
                } else {
                    Log.d("visble", "no");
                }
            }
        } finally {


            if (hold_viewHeight.size() > 0) {
                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
                Collections.sort(hold_viewHeight);
                Log.d("old array acc order", hold_viewHeight.toString());
                Log.d("temp array a order", hold_TempviewHeight.toString());

                int getViewID = 0;
                Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 3)));
                int heights = hold_viewHeight.get(hold_viewHeight.size() - 3);
                Log.d("heights", String.valueOf(heights));
                for (int position = 0; position <= hold_TempviewHeight.size(); position++) {
                    if (heights == hold_TempviewHeight.get(position)) {
                        getViewID = hold_viewID.get(position);
                        break;
                    }
                }
                selected_position = getViewID;
                ScrollViewPosition.scrollToDeepChild(getViewID);
            }

        }
    }
    final int finalSelected_position = selected_position;
    mListviewUsers.post(new Runnable() {
        @Override
        public void run() {

            if (finalSelected_position == 0) {
                mListviewUsers.smoothScrollToPosition(Classes.put_user_name.size());

            } else {
                mListviewUsers.smoothScrollToPosition(finalSelected_position - 1);
            }

        }
    });
    Handler handler = new Handler();
    Runnable r = new Runnable() {

        @Override
        public void run() {
            updateListViewBackgroundColors();
        }
    };
    handler.postDelayed(r, 250);

          /*      Intent intentUpdate = new Intent();
                intentUpdate.setAction("action_key");
                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                intentUpdate.putExtra("extract_key", String.valueOf("00000"));
                mActivity.sendBroadcast(intentUpdate);*/
}

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    private void creeateView(int ii, int defulte_Selected) {

        if (ii == 0) {
            linearLayoutFragments.removeAllViews();
        }
        gloable_array = new ArrayList<>();
        Classes.addUserName();
        Classes.setClearUserPositionInfomrmation();
        Classes.addUserPic();
        linearLayoutFragments = (LinearLayout) findViewById(R.id.layout_fragments);
        /// setUserActive();
        for (int i = 0; i < Classes.put_user_name.size(); i++) {
            String value_colors = String.valueOf(getRandomColor());
            final LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            frameLayout = new FrameLayout(Main2ActivityDemo2.this);
            frameLayout.setId(i + 1);
            frameLayout.setTag(i + 1);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("position", Classes.getUserName(i));
            args.putString("colors", value_colors);
            args.putString("user_url", Classes.getUserUrl(i));
            Fragment1 fragmentHome = new Fragment1();
            fragmentHome.setArguments(args);
            fragmentTransaction.add(frameLayout.getId(), fragmentHome);
            fragmentTransaction.commit();


            frameLayout.setOnClickListener(new View.OnClickListener() {
                public int ip;

                @Override
                public void onClick(View v) {
                    ip++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            ip = 0;
                        }
                    };

                    if (ip == 1) {
                        //Single click
                        handler.postDelayed(r, 250);
                    } else if (ip == 2) {
                        //Double click
                        ip = 0;

                        Intent inten = new Intent(getApplicationContext(), FullScreenChat.class);
                        inten.putExtra("user_id", "1");
                        inten.putExtra("user_bg_colors", "345435");
                        startActivity(inten);
                    }
                }

            });

            gloable_array.add("0");


            HashMap<String, String> user_object = new HashMap<>();
            user_object.put("user_holder_background_active", "0");
            user_object.put("user_holder_id", Classes.getUserName(i));

            user_object.put("user_holder_user_url", Classes.getUserUrl(i));
            Classes.addUserInformation(user_object);


            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    linearLayoutFragments.addView(frameLayout);
                }
            });
        }

        Intent intentUpdate = new Intent();
        intentUpdate.setAction("action_key");
        intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
        intentUpdate.putExtra("extract_key", String.valueOf(1));
        mActivity.sendBroadcast(intentUpdate);
        adapter = new SimpleAdapter(mActivity, Classes.UserPositionInfomrmation);
        mListviewUsers.setAdapter(adapter);


        if (ii == 0) {
            ////
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {

                            progress.dismiss();

                        }
                    },
                    1000
            );
        }
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 100);
    }


    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;

        private ArrayList<HashMap<String, String>> UserPositionInfomrmation;

        public SimpleAdapter(Activity mActivity, ArrayList<HashMap<String, String>> UserPositionInfomrmation) {
            this.mActivity = mActivity;

            this.UserPositionInfomrmation = UserPositionInfomrmation;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return UserPositionInfomrmation.size();
        }

        @Override
        public Object getItem(int position) {
            return UserPositionInfomrmation.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.items_users, null);
                holder = new ViewHolder();
                holder.mImageViewUsersPic = (ImageView) convertView.findViewById(R.id.ImageViewUsersPic);
                holder.mTextviewpostition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.LinearLayoutItmabg = (LinearLayout) convertView.findViewById(R.id.LinearLayoutItem_bg);
                holder.LinearLayoutperfectItmeScreen = (LinearLayout) convertView.findViewById(R.id.LinearLayoutperfectItmeScreen);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ////now getting the User information here

            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = Classes.getUserInfoByPosition(position);
            ///  selectedItem.get("user_holder_background_active").toString()
            ///gloable_array
            if (selectedItem.get("user_holder_background_active").toString().toString().equalsIgnoreCase("0")) {
                holder.LinearLayoutItmabg.setBackgroundColor(getResources().getColor(R.color.black));
                holder.mTextviewpostition.setTextColor(Color.BLACK);
            } else {
                holder.LinearLayoutItmabg.setBackgroundColor(getResources().getColor(R.color.white));
                holder.LinearLayoutItmabg.setBackgroundColor(Color.GRAY);
            }
            Picasso.with(getApplicationContext()).load(selectedItem.get("user_holder_user_url").toString())
                    .resize(100, 100).transform(new CircleTransform())
                    .centerCrop().placeholder(R.drawable.defult_users)
                    .into(holder.mImageViewUsersPic);

            int result = Classes.getProgamaticalHeights(false, mActivity) - 150;
            /// holder.LinearLayoutperfectItmeScreen.getLayoutParams().height = result / 7;
            holder.mTextviewpostition.setText(selectedItem.get("user_holder_id").toString());


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ScrollViewPosition.post(new Runnable() {

                        public void run() {
                            Classes.getUserInformationLog();
                            try {
                                ScrollViewPosition.scrollToDeepChild(position + 1);
                                Intent intentUpdate = new Intent();
                                intentUpdate.setAction("action_key");
                                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                                intentUpdate.putExtra("extract_key", String.valueOf(position + 1));
                                mActivity.sendBroadcast(intentUpdate);


                            } finally {
                                Handler handler = new Handler();
                                Runnable r = new Runnable() {
                                    @Override
                                    public void run() {
                                        updateListViewBackgroundColors();
                                    }
                                };
                                handler.postDelayed(r, 250);
                            }


                        }
                    });
                }
            });
            return convertView;
        }
    }


    public void updateListViewBackgroundColors() {
        int Current_user_position = 0;
        ArrayList<Integer> visisble_view_index = new ArrayList<>();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int gloable_reduce_height = 150 + Fragment1.getStatusBarHeight(mActivity);
        int result = height - gloable_reduce_height;
        try {
            for (int i = 0; i < Classes.put_user_name.size(); i++) {
                Object viewID = i;

                View convertView = findViewById(i + 1);
                Log.d("id", convertView.toString());


                Rect scrollBounds = new Rect();
                ScrollViewPosition.getDrawingRect(scrollBounds);
                if (convertView.getLocalVisibleRect(scrollBounds)) {
                    Log.d("HEIGHT       :", String.valueOf(scrollBounds.height()));
                    Log.d("visble", "yes");
                    Log.d("id name", String.valueOf(i + 1));
                    ///  gloable_array.set(i,"1");
                    Log.d("convertView", String.valueOf(scrollBounds.height()));
                    if (auto_selected_panal == 2) {
                        if (result / 3 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                            Current_user_position = i;
                        }
                    } else if (auto_selected_panal == 1) {
                        if (result / 2 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    } else if (auto_selected_panal == 0) {
                        if (result == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    }


                } else {
                    // NONE of the imageView is within the visible window
                    Log.d("visble", "no");
                }


            }
            gloable_array.clear();
            for (int i = 0; i < Classes.put_user_name.size(); i++) {
                if (visisble_view_index.contains(i)) {
                    gloable_array.add("1");

                } else {
                    gloable_array.add("0");
                }
            }


        } finally {
            adapter.notifyDataSetChanged();
        }


        for (int position = 0; position < Classes.put_user_name.size(); position++) {
            /////update user information
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = Classes.getUserInfoByPosition(position);
            String value_user_holder_id = selectedItem.get("user_holder_id").toString();
            String value_user_holder_user_url = selectedItem.get("user_holder_user_url").toString();

            String active_users;
            if (visisble_view_index.contains(position)) {
                gloable_array.add("1");
                active_users = "1";

            } else {
                gloable_array.add("0");
                active_users = "0";
            }
            HashMap<String, String> query_update = new HashMap<String, String>();
            query_update.put("user_holder_background_active", active_users);
            query_update.put("user_holder_id", value_user_holder_id);
            query_update.put("user_holder_user_url", value_user_holder_user_url);
            Classes.updateUserPositionInfomrmation(position, query_update);


        }
        adapter.notifyDataSetChanged();


    }

    public class ViewHolder {
        ImageView mImageViewUsersPic;
        TextView mTextviewpostition;
        LinearLayout LinearLayoutItmabg, LinearLayoutperfectItmeScreen;
    }

    public static ShapeDrawable drawCircle(Context context, int width, int height, int color, int i) {
        ShapeDrawable oval;
        if (i == 0) {
            oval = new ShapeDrawable(new OvalShape());
        } else {
            oval = new ShapeDrawable(new RectShape());
        }
        oval.setIntrinsicHeight(height);
        oval.setIntrinsicWidth(width);
        oval.getPaint().setColor(color);
        return oval;
    }

    private boolean isViewVisible(View view) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isViewVisiblet(View view) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isViewVisbleActual(final View view, final int viewID) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);
        if (view.getLocalVisibleRect(scrollBounds)) {
            Log.d("WIDTH        :", String.valueOf(scrollBounds.width()));
            Log.d("HEIGHT       :", String.valueOf(scrollBounds.height()));
            Log.d("left         :", String.valueOf(scrollBounds.left));
            Log.d("right        :", String.valueOf(scrollBounds.right));
            Log.d("top          :", String.valueOf(scrollBounds.top));
            Log.d("bottom       :", String.valueOf(scrollBounds.bottom));
            hold_viewID.add(viewID);
            hold_viewHeight.add(scrollBounds.height());
            hold_TempviewHeight.add(scrollBounds.height());
            return true;
        } else {
            // NONE of the imageView is within the visible window
            return false;
        }
    }

    public interface onActualVisibleViewHeight {
        void catchingIndex(String s);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 250);
    }
    public void setIntentUpdateBroadCast() {
        mUpdateBroadCast = new UpdateBroadCast();
        IntentFilter intentFilter_update = new IntentFilter("update_ui_action");
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        mActivity.registerReceiver(mUpdateBroadCast, intentFilter_update);
    }
    class UpdateBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
           Toast.makeText(mActivity,"1",Toast.LENGTH_LONG).show();
            updateView();
        }
    }
}
