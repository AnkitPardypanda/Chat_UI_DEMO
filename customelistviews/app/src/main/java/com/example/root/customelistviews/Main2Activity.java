package com.example.root.customelistviews;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

public class Main2Activity extends FragmentActivity {

    private int auto_selected_panal = 0;
    LinearLayout linearLayoutFragments;
    private ListView mListviewUsers;
    private LinearLayout mLinearLayoutHeader;
    private TextView TextviewOne;
    private TextView TextviewTwo;
    private TextView TextviewThree;
    private ArrayList<Fragment> fragList;
    private DeepScrollView ScrollViewPosition;

    private Activity mActivity;
    private FrameLayout frameLayout;
    private ProgressDialog progress = null;

    private ArrayList<Integer> hold_viewID = new ArrayList<>();
    private ArrayList<Integer> hold_viewHeight = new ArrayList<>();
    private ArrayList<Integer> hold_TempviewHeight = new ArrayList<>();
    private ArrayList<String> user_holder_id, user_holder_bg, user_holder_color_code, gloable_array;
    private SimpleAdapter adapter;
    ArrayList<Integer> gloable_exiting_view_id = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mActivity = this;
        mListviewUsers = (ListView) findViewById(R.id.ListviewUsers);
        mLinearLayoutHeader = (LinearLayout) findViewById(R.id.LinearLayoutHeader);
        ScrollViewPosition = (DeepScrollView) findViewById(R.id.ScrollViewPosition);
        TextviewOne = (TextView) findViewById(R.id.TextviewOne);
        TextviewTwo = (TextView) findViewById(R.id.TextviewTwo);
        TextviewThree = (TextView) findViewById(R.id.TextviewThree);
        fragList = new ArrayList<Fragment>();
        mListviewUsers.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        mLinearLayoutHeader.getLayoutParams().height = 150;
        creeateView(1,1);

        updateListViewBackgroundColors();




        ScrollViewPosition.setOnScrollViewListener(new DeepScrollView.OnScrollViewListener() {
            @Override
            public void onScrollChanged(DeepScrollView v, int l, int t, int oldl, int oldt) {

                ////
                //// Toast.makeText(getApplication(),"Ankit",Toast.LENGTH_LONG).show();
            }
        });


        ScrollViewPosition.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ScrollViewPosition.startScrollerTask();
                }
                return false;
            }
        });
        ScrollViewPosition.setOnScrollStoppedListener(new DeepScrollView.OnScrollStoppedListener() {


            @Override
            public void onScrollStopped() {

int selected_position=0;

                if (auto_selected_panal == 0) {
                /////    Toast.makeText(mActivity, "single chat", Toast.LENGTH_LONG).show();

                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i <= 16; i++) {
                            Object viewID = i;

                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isViewVisbleActual(convertView, i)) {
                                Log.d("visble", "yes");

                            } else {
                                Log.d("visble", "no");
                            }
                        }
                    } finally {
                        if (hold_viewHeight.size() > 0) {
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));

                            Integer i = Collections.max(hold_viewHeight);
                            Log.d("height", String.valueOf(i));
                            int getViewID = 0;
                            for (int position = 0; position <= hold_viewHeight.size(); position++) {
                                if (i == hold_viewHeight.get(position)) {
                                    getViewID = hold_viewID.get(position);
                                    break;
                                }
                            }
                            selected_position=getViewID;
                            ScrollViewPosition.scrollToDeepChild(getViewID);
                        }
                      }
                } else if (auto_selected_panal == 1) {
                    /////  Toast.makeText(mActivity, "Double chat", Toast.LENGTH_LONG).show();

                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i <=16; i++) {
                            Object viewID = i;

                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isViewVisbleActual(convertView, i)) {
                                Log.d("visble", "yes");
                            } else {
                                Log.d("visble", "no");
                            }
                        }
                    } finally {
                        Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                        Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                        Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                        Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
                        Collections.sort(hold_viewHeight);
                        Log.d("old array acc order", hold_viewHeight.toString());
                        Log.d("temp array a order", hold_TempviewHeight.toString());

                        if (hold_viewHeight.size() == 1) {

                        } else {
                            if (hold_viewHeight.size() > 0) {
                                int getViewID = 0;
                                Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 2)));
                                int heights = hold_viewHeight.get(hold_viewHeight.size() - 2);
                                Log.d("heights", String.valueOf(heights));
                                for (int position = 0; position <= hold_TempviewHeight.size(); position++) {
                                    if (heights == hold_TempviewHeight.get(position)) {
                                        getViewID = hold_viewID.get(position);
                                        break;
                                    }
                                }
                                selected_position=getViewID;
                                ScrollViewPosition.scrollToDeepChild(getViewID);
                            }
                        }


                    }


                } else if (auto_selected_panal == 2) {
                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i <= 16; i++) {
                            Object viewID = i;

                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isViewVisbleActual(convertView, i)) {
                                Log.d("visble", "yes");

                            } else {
                                Log.d("visble", "no");
                            }
                        }
                    } finally {


                        if (hold_viewHeight.size() > 0) {
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
                            Collections.sort(hold_viewHeight);
                            Log.d("old array acc order", hold_viewHeight.toString());
                            Log.d("temp array a order", hold_TempviewHeight.toString());

                            int getViewID = 0;
                            Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 3)));
                            int heights = hold_viewHeight.get(hold_viewHeight.size() - 3);
                            Log.d("heights", String.valueOf(heights));
                            for (int position = 0; position <= hold_TempviewHeight.size(); position++) {
                                if (heights == hold_TempviewHeight.get(position)) {
                                    getViewID = hold_viewID.get(position);
                                    break;
                                }
                            }
                            selected_position=getViewID;
                            ScrollViewPosition.scrollToDeepChild(getViewID);
                        }

                    }
                }
                final int finalSelected_position = selected_position;
                mListviewUsers.post(new Runnable() {
                    @Override
                    public void run() {
                        mListviewUsers.smoothScrollToPosition(finalSelected_position);
                    }
                });
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        updateListViewBackgroundColors();
                    }
                };
                handler.postDelayed(r, 250);

                Intent intentUpdate = new Intent();
                intentUpdate.setAction("action_key");
                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                intentUpdate.putExtra("extract_key", String.valueOf("00000"));
                mActivity.sendBroadcast(intentUpdate);
            }
        });
        TextviewOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 0;
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.show();
                fragList.clear();
                Classes.Screeen_size = 0;
                creeateView(0,1);
            }
        });
        TextviewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 1;
                Classes.Screeen_size = 1;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                creeateView(0,2);
            }
        });




        TextviewThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

  /*              auto_selected_panal = 2;
                Classes.Screeen_size = 2;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                creeateView(0,3);*/
                Classes.setArrayFirstTime();
                Classes.setTopPossition(3);
                Classes.addToTop("3");
                Classes.AddButttom(0,"3");

            }
        });


/*

         sortList = new ArrayList<HashMap<String, String>>();
        for (int ii = 1; ii <= 16; ii++) {
            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", String.valueOf(getRandomColor()));
            selectedItem1.put("position", String.valueOf(ii));
            selectedItem1.put("bg", "0");
            sortList.ad d(selectedItem1);
        }
        SimpleAdapter adapter = new SimpleAdapter(this, sortList);
        mListviewUsers.setAdapter(adapter);


        */


    }


    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    private void creeateView(int ii,int defulte_Selected) {

        if (ii == 0) {
            linearLayoutFragments.removeAllViews();
        }
        gloable_array = new ArrayList<>();
        user_holder_id = new ArrayList<>();
        user_holder_bg = new ArrayList<>();
Classes.setClearUserPositionInfomrmation();
        user_holder_color_code = new ArrayList<>();
        linearLayoutFragments = (LinearLayout) findViewById(R.id.layout_fragments);
       /// setUserActive();
        for (int i = 1; i <= 16; i++) {
            String value_colors = String.valueOf(getRandomColor());
            final LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            frameLayout = new FrameLayout(Main2Activity.this);
            frameLayout.setId(i);
            frameLayout.setTag(i);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("position", String.valueOf(i));
            args.putString("colors", value_colors);
            args.putString("user_url", "");
            Fragment1 fragmentHome = new Fragment1();
            fragmentHome.setArguments(args);
            fragmentTransaction.add(frameLayout.getId(), fragmentHome);
            fragmentTransaction.commit();
            //////////textview


        /*

            HashMap<String, String> selectedItem1 = new HashMap<String, String>();
            selectedItem1.put("color_code", value_colors);
            selectedItem1.put("position", String.valueOf(ii));
            selectedItem1.put("bg", "0");
            sortList.add(selectedItem1);

            */


            frameLayout.setOnClickListener(new View.OnClickListener() {
                public int ip;

                @Override
                public void onClick(View v) {
                    ip++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            ip = 0;
                        }
                    };

                    if (ip == 1) {
                        //Single click
                        handler.postDelayed(r, 250);
                    } else if (ip == 2) {
                        //Double click
                        ip = 0;

                        Intent inten = new Intent(getApplicationContext(), FullScreenChat.class);
                        inten.putExtra("user_id", "1");
                        inten.putExtra("user_bg_colors", "345435");
                        startActivity(inten);
                    }
                }

            });

/*
if (defulte_Selected==1){
if (i==1){
    gloable_array.add("1");
}else {
    gloable_array.add("0");
}
}else if (defulte_Selected==2){
    if (i==1){
        gloable_array.add("1");
    }else if (i==2){
        gloable_array.add("1");
    }else {
        gloable_array.add("0");
    }
}else {
    if (i==1){
        gloable_array.add("1");
    }else if (i==2){
        gloable_array.add("1");
    }else if (i==3){
        gloable_array.add("1");
    }else {
        gloable_array.add("0");
    }
}
*/


            user_holder_id.add(String.valueOf(i));
            user_holder_color_code.add(value_colors);

            gloable_array.add("0");
            user_holder_bg.add("0");

            HashMap<String,String> user_object=new HashMap<>();
            user_object.put("user_background","0");
            Classes.addUserInformation(user_object);




            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    linearLayoutFragments.addView(frameLayout);
                }
            });
        }

        Intent intentUpdate = new Intent();
        intentUpdate.setAction("action_key");
        intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
        intentUpdate.putExtra("extract_key", String.valueOf(1));
        mActivity.sendBroadcast(intentUpdate);
        adapter = new SimpleAdapter(mActivity, user_holder_id, user_holder_bg, user_holder_color_code);
        mListviewUsers.setAdapter(adapter);




        if (ii == 0) {
            ////
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {

                            progress.dismiss();

                               }
                    },
                    1000
            );
        }
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 100);
    }


    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;
        private ArrayList<String> user_id, user_bg, user_colors;

 public SimpleAdapter(Activity mActivity, ArrayList<String> user_id, ArrayList<String> user_bg, ArrayList<String> user_colors) {
            this.mActivity = mActivity;
            this.user_id = user_id;
            this.user_bg = user_bg;
            this.user_colors = user_colors;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return user_id.size();
        }

        @Override
        public Object getItem(int position) {
            return user_id.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.items_users, null);
                holder = new ViewHolder();
                holder.mTextviewpostition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.LinearLayoutItmabg = (LinearLayout) convertView.findViewById(R.id.LinearLayoutItem_bg);
                holder.LinearLayoutperfectItmeScreen=(LinearLayout)convertView.findViewById(R.id.LinearLayoutperfectItmeScreen);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ////now getting the User information here
            ArrayList<HashMap<String,String>> UserPositionInfomrmation = new ArrayList<HashMap<String,String>>();
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem =Classes.UserPositionInfomrmation.get(position);
       String user_backgroundObject=     selectedItem.get("user_background").toString();



            if (gloable_array.get(position).toString().equalsIgnoreCase("0")) {
                holder.LinearLayoutItmabg.setBackgroundColor(Color.WHITE);
            } else {
                holder.LinearLayoutItmabg.setBackgroundColor(Color.GRAY);
            }
            int result = Classes.getProgamaticalHeights(false, mActivity) - 150;
            holder.LinearLayoutperfectItmeScreen.getLayoutParams().height=result/7;
            holder.mTextviewpostition.setText(user_id.get(position));
            if (Build.VERSION.SDK_INT >= 16) {
                holder.mTextviewpostition.setBackground(drawCircle(mActivity, 50, 50, Integer.parseInt(user_colors.get(position)), 0));
            } else {
                holder.mTextviewpostition.setBackgroundDrawable(drawCircle(mActivity, 50, 50, Integer.parseInt(user_colors.get(position)), 0));
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ScrollViewPosition.post(new Runnable() {

                        public void run() {
Classes.getUserInformationLog();
                            try {
                                ScrollViewPosition.scrollToDeepChild(position + 1);
                                Intent intentUpdate = new Intent();
                                intentUpdate.setAction("action_key");
                                intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
                                intentUpdate.putExtra("extract_key", String.valueOf(position + 1));
                                mActivity.sendBroadcast(intentUpdate);
                            } finally {
                                Handler handler = new Handler();
                                Runnable r = new Runnable() {
                                 @Override
                                    public void run() {
                                        updateListViewBackgroundColors();
                                    }
                                };
                                handler.postDelayed(r, 250);
                                     }


                        }
                    });
                }
            });
            return convertView;
        }
    }


    public void updateListViewBackgroundColors() {
        int Current_user_position = 0;
        ArrayList<Integer> visisble_view_index = new ArrayList<>();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int result = height - 150;
        try {
            for (int i = 1; i <= 16; i++) {
                Object viewID = i;

                View convertView = findViewById(i);
                Log.d("id", convertView.toString());


                Rect scrollBounds = new Rect();
                ScrollViewPosition.getDrawingRect(scrollBounds);
                if (convertView.getLocalVisibleRect(scrollBounds)) {
                    Log.d("HEIGHT       :", String.valueOf(scrollBounds.height()));
                    Log.d("visble", "yes");
                    Log.d("id name", String.valueOf(i));
                    ///  gloable_array.set(i,"1");
                    Log.d("convertView", String.valueOf(scrollBounds.height()));
                    if (auto_selected_panal == 2) {
                        if (result / 3 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                            Current_user_position = i;
                        }
                    } else if (auto_selected_panal == 1) {
                        if (result / 2 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    } else if (auto_selected_panal == 0) {
                        if (result == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    }


                } else {
                    // NONE of the imageView is within the visible window
                    Log.d("visble", "no");
                }






             /*

              if (isViewVisbleActual(convertView, i)) {
                    Log.d("visble", "yes");
                    Log.d("id name",String.valueOf(i));
                  ///  gloable_array.set(i,"1");
                    Log.d("convertView",String.valueOf(convertView.getHeight()));


                    visisble_view_index.add(i);
                } else {
                    Log.d("visble", "no");
                  ///  gloable_array.set(i,"0");

                }


                */

            }
            gloable_array.clear();
            for (int i = 1; i <= 16; i++) {
                if (visisble_view_index.contains(i)) {
                    gloable_array.add("1");
                } else {
                    gloable_array.add("0");
                }
            }
            ////////Update User Information

            for (int i = 1; i <16; i++) {

                if (visisble_view_index.contains(i)) {
                    HashMap<String, String> selectedItem = new HashMap<String, String>();

                    selectedItem.put("user_background","1");

                    Classes.updateUserPositionInfomrmation(i,selectedItem);

                } else {
                    HashMap<String, String> selectedItem = new HashMap<String, String>();
                    selectedItem.put("user_background","0");
                    Classes.updateUserPositionInfomrmation(i,selectedItem);
                }
            }






        } finally {
          adapter.notifyDataSetChanged();
        }




/*

        for (int i=0;i<gloable_array.size();i++){
            if (position==i){
                gloable_array.set(i,"1");
            }
            else {
                gloable_array.set(i,"0");
            }
        }*/
        adapter.notifyDataSetChanged();

    }

    public class ViewHolder {

        TextView mTextviewpostition;
        LinearLayout LinearLayoutItmabg,LinearLayoutperfectItmeScreen;
    }

    public static ShapeDrawable drawCircle(Context context, int width, int height, int color, int i) {
        ShapeDrawable oval;
        if (i == 0) {
            oval = new ShapeDrawable(new OvalShape());
        } else {
            oval = new ShapeDrawable(new RectShape());
        }
        oval.setIntrinsicHeight(height);
        oval.setIntrinsicWidth(width);
        oval.getPaint().setColor(color);
        return oval;
    }

    private boolean isViewVisible(View view) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isViewVisiblet(View view) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isViewVisbleActual(final View view, final int viewID) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);
        if (view.getLocalVisibleRect(scrollBounds)) {
            Log.d("WIDTH        :", String.valueOf(scrollBounds.width()));
            Log.d("HEIGHT       :", String.valueOf(scrollBounds.height()));
            Log.d("left         :", String.valueOf(scrollBounds.left));
            Log.d("right        :", String.valueOf(scrollBounds.right));
            Log.d("top          :", String.valueOf(scrollBounds.top));
            Log.d("bottom       :", String.valueOf(scrollBounds.bottom));
            hold_viewID.add(viewID);
            hold_viewHeight.add(scrollBounds.height());
            hold_TempviewHeight.add(scrollBounds.height());
            return true;
        } else {
            // NONE of the imageView is within the visible window
            return false;
        }
    }

    public interface onActualVisibleViewHeight {
        void catchingIndex(String s);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 250);
    }
}
