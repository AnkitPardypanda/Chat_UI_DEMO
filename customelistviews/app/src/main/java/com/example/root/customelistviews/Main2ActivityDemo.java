package com.example.root.customelistviews;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Main2ActivityDemo extends FragmentActivity {

    private int auto_selected_panal = 0;
    private LinearLayout linearLayoutFragments;
    private ListView mListviewUsers;
    private LinearLayout mLinearLayoutHeader;
    private TextView TextviewOne;
    private TextView TextviewTwo;
    private TextView TextviewThree;
    private ArrayList<Fragment> fragList;
    private DeepScrollView ScrollViewPosition;
    public static int size = 0;
    private Activity mActivity;
    private FrameLayout frameLayout;
    private ProgressDialog progress = null;

    private ArrayList<Integer> hold_viewID = new ArrayList<>();
    private ArrayList<Integer> hold_viewHeight = new ArrayList<>();
    private ArrayList<Integer> hold_TempviewHeight = new ArrayList<>();
    private ArrayList<String> gloable_array;
    private SimpleAdapter adapter;
    ArrayList<Integer> gloable_exiting_view_id = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mActivity = this;
        mListviewUsers = (ListView) findViewById(R.id.ListviewUsers);
        mLinearLayoutHeader = (LinearLayout) findViewById(R.id.LinearLayoutHeader);
        ScrollViewPosition = (DeepScrollView) findViewById(R.id.ScrollViewPosition);
        TextviewOne = (TextView) findViewById(R.id.TextviewOne);
        TextviewTwo = (TextView) findViewById(R.id.TextviewTwo);
        TextviewThree = (TextView) findViewById(R.id.TextviewThree);
        fragList = new ArrayList<Fragment>();
        mLinearLayoutHeader.getLayoutParams().height = Classes.screen_heights;
        setCreateView(1, 1);
        updateListViewBackgroundColors();
        Classes.initBroadCast(new UpdateBroadCast(), mActivity, "update_ui_action");
        Classes.initBroadCast(new ScrollviewUpdate(), mActivity, "childShowButtom");


        ScrollViewPosition.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ScrollViewPosition.startScrollerTask();
                }
                return false;
            }
        });
        ScrollViewPosition.setOnScrollStoppedListener(new DeepScrollView.OnScrollStoppedListener() {


            @Override
            public void onScrollStopped() {
                Classes.sendBroadCast(mActivity, "childShowButtom", "2");
                updateView();

            }
        });
        TextviewOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 0;
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.show();
                fragList.clear();
                Classes.Screeen_size = 0;
                setCreateView(0, 1);
            }
        });
        TextviewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_selected_panal = 1;
                Classes.Screeen_size = 1;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                setCreateView(0, 2);

            }
        });


        TextviewThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                auto_selected_panal = 2;
                Classes.Screeen_size = 2;
                linearLayoutFragments.removeAllViews();
                progress = new ProgressDialog(mActivity);
                progress.setMessage("Loading...");
                progress.show();
                fragList.clear();
                setCreateView(0, 3);
               /* Classes.setArrayFirstTime();
                Classes.setTopPossition(3);
                Classes.addToTop("3");
                Classes.AddButttom(0, "3");*/

            }
        });


    }

    public void updateView() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                int selected_position = 0;

                if (auto_selected_panal == 0) {


                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i < Classes.put_user_name.size(); i++) {
                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isChatShowingActive(convertView, i)) {
                                Log.d("visible", "yes");
                            } else {
                                Log.d("visible", "no");
                            }
                        }
                    } finally {
                        if (hold_viewHeight.size() > 0) {
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                            Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                            Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));

                            Integer i = Collections.max(hold_viewHeight);
                            Log.d("height", String.valueOf(i));
                            int getViewID = 0;
                            for (int position = 0; position <= hold_viewHeight.size(); position++) {
                                if (i == hold_viewHeight.get(position)) {
                                    getViewID = hold_viewID.get(position);
                                    break;
                                }
                            }
                            selected_position = getViewID;

                            final int finalSelected_position1 = selected_position;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ScrollViewPosition.scrollToDeepChild(finalSelected_position1);
                                }
                            });

                        }
                    }
                } else if (auto_selected_panal == 1) {
                    /////  Toast.makeText(mActivity, "Double chat", Toast.LENGTH_LONG).show();

                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i < Classes.put_user_name.size(); i++) {
                            Object viewID = i;

                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isChatShowingActive(convertView, i)) {
                                Log.d("visble", "yes");
                            } else {
                                Log.d("visble", "no");
                            }
                        }
                    } finally {
                        Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                        Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                        Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                        Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
                        Collections.sort(hold_viewHeight);
                        Log.d("old array acc order", hold_viewHeight.toString());
                        Log.d("temp array a order", hold_TempviewHeight.toString());

                        if (hold_viewHeight.size() == 1) {


                        } else {
                            if (hold_viewHeight.size() > 0) {
                                int getViewID = 0;
                                Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 2)));
                                int heights = hold_viewHeight.get(hold_viewHeight.size() - 2);
                                Log.d("heights", String.valueOf(heights));
                                for (int position = 0; position < hold_TempviewHeight.size(); position++) {
                                    if (heights == hold_TempviewHeight.get(position)) {
                                        getViewID = hold_viewID.get(position);
                                        break;
                                    }
                                }
                                selected_position = getViewID;

                                final int finalGetViewID = selected_position;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ScrollViewPosition.scrollToDeepChild(finalGetViewID);
                                    }
                                });

                            }
                        }
                    }


                } else if (auto_selected_panal == 2) {
                    /////////////three chat open here

                    if (!hold_viewHeight.isEmpty()) {
                        hold_viewHeight.clear();
                        hold_viewID.clear();
                        hold_TempviewHeight.clear();
                    }
                    try {
                        for (int i = 1; i < Classes.put_user_name.size(); i++) {
                            Object viewID = i;

                            View convertView = findViewById(i);
                            Log.d("id", convertView.toString());
                            if (isChatShowingActive(convertView, i)) {
                                Log.d("visble", "yes");
                            } else {
                                Log.d("visble", "no");
                            }
                        }
                    } finally {


                        try {
                            if (hold_viewHeight.size() > 0) {
                                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.size()));
                                Log.d("hold_viewID", String.valueOf(hold_viewID.size()));
                                Log.d("hold_viewHeight", String.valueOf(hold_viewHeight.toString()));
                                Log.d("hold_viewID", String.valueOf(hold_viewID.toString()));
                                Collections.sort(hold_viewHeight);
                                Log.d("old array acc order", hold_viewHeight.toString());
                                Log.d("temp array a order", hold_TempviewHeight.toString());

                                int getViewID = 0;
                                Log.d("old last array acc or", String.valueOf(hold_viewHeight.get(hold_viewHeight.size() - 4)));
                                int heights = hold_viewHeight.get(hold_viewHeight.size() - 4);
                                Log.d("heights", String.valueOf(heights));
                                for (int position = 0; position <= hold_TempviewHeight.size(); position++) {
                                    if (heights == hold_TempviewHeight.get(position)) {
                                        getViewID = hold_viewID.get(position);
                                        break;
                                    }
                                }
                                selected_position = getViewID;

                                final int finalSelected_position2 = selected_position;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ScrollViewPosition.scrollToDeepChild(finalSelected_position2);
                                    }
                                });

                            }

                        } catch (Exception e) {

                        }
                    }
                }
                final int finalSelected_position = selected_position;
                Log.d("position", String.valueOf(finalSelected_position));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListviewUsers.post(new Runnable() {
                            @Override
                            public void run() {

                                if (finalSelected_position == 0) {
                                    mListviewUsers.smoothScrollToPosition(Classes.put_user_name.size());

                                } else {
                                    mListviewUsers.smoothScrollToPosition(finalSelected_position - 1);
                                }

                            }
                        });
                        Handler handler = new Handler();
                        Runnable r = new Runnable() {

                            @Override
                            public void run() {
                                updateListViewBackgroundColors();
                            }
                        };
                        handler.postDelayed(r, 250);

                        Classes.sendBroadCast(mActivity, "editText_hide_action", String.valueOf(finalSelected_position));
                    }
                });


                //Do things...
                return null;
            }
        }.execute();


    }

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    private void setCreateView(int ii, int default_Selected) {

        if (ii == 0) {
            linearLayoutFragments.removeAllViews();
        }
        gloable_array = new ArrayList<>();
        Classes.addUserName();
        Classes.setClearUserPositionInfomrmation();
        Classes.addUserPic();
        linearLayoutFragments = (LinearLayout) findViewById(R.id.layout_fragments);
        /// setUserActive();
        for (int i = 0; i < Classes.put_user_name.size(); i++) {
            String value_colors = String.valueOf(getRandomColor());
            final LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            frameLayout = new FrameLayout(Main2ActivityDemo.this);
            frameLayout.setId(i + 1);
            frameLayout.setTag(i + 1);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("position", Classes.getUserName(i));
            args.putString("colors", value_colors);
            args.putString("user_url", Classes.getUserUrl(i));
            args.putString("fragment_id", String.valueOf(i + 1));
            Fragment1 fragmentHome = new Fragment1();
            fragmentHome.setArguments(args);
            fragmentTransaction.add(frameLayout.getId(), fragmentHome);
            fragmentTransaction.commit();


            frameLayout.setOnClickListener(new View.OnClickListener() {
                public int ip;

                @Override
                public void onClick(View v) {
                    ip++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            ip = 0;
                        }
                    };

                    if (ip == 1) {
                        // Single click
                        handler.postDelayed(r, 250);
                    } else if (ip == 2) {
                        // Double click
                        ip = 0;
                        Intent inten = new Intent(getApplicationContext(), FullScreenChat.class);
                        inten.putExtra("user_id", "1");
                        inten.putExtra("user_bg_colors", "345435");
                        startActivity(inten);
                    }
                }

            });

            gloable_array.add("0");


            HashMap<String, String> user_object = new HashMap<>();
            user_object.put("user_holder_background_active", "0");
            user_object.put("user_holder_id", Classes.getUserName(i));
            user_object.put("user_holder_user_url", Classes.getUserUrl(i));
            Classes.addUserInformation(user_object);


            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    linearLayoutFragments.addView(frameLayout);
                }
            });
        }


        adapter = new SimpleAdapter(mActivity, Classes.UserPositionInfomrmation);
        mListviewUsers.setAdapter(adapter);


        if (ii == 0) {
            ////
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {

                            progress.dismiss();

                        }
                    },
                    1000
            );
        }
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 100);
    }


    public class SimpleAdapter extends BaseAdapter {
        private Activity mActivity;
        private LayoutInflater layoutInflater;

        private ArrayList<HashMap<String, String>> UserPositionInfomrmation;

        public SimpleAdapter(Activity mActivity, ArrayList<HashMap<String, String>> UserPositionInfomrmation) {
            this.mActivity = mActivity;
            this.UserPositionInfomrmation = UserPositionInfomrmation;
            layoutInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return UserPositionInfomrmation.size();
        }

        @Override
        public Object getItem(int position) {
            return UserPositionInfomrmation.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.items_users, null);
                holder = new ViewHolder();
                holder.mImageViewUsersPic = (ImageView) convertView.findViewById(R.id.ImageViewUsersPic);
                holder.mTextViewPosition = (TextView) convertView.findViewById(R.id.Textviewpostition);
                holder.mLinearLayoutItem_bg = (LinearLayout) convertView.findViewById(R.id.LinearLayoutItem_bg);
                holder.mLinearLayoutPerfectItemScreen = (LinearLayout) convertView.findViewById(R.id.LinearLayoutperfectItmeScreen);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            //// Now getting the User information here

            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = Classes.getUserInfoByPosition(position);
            ///  selectedItem.get("user_holder_background_active").toString()
            ///gloable_array
            if (selectedItem.get("user_holder_background_active").toString().toString().equalsIgnoreCase("0")) {
                holder.mLinearLayoutItem_bg.setBackgroundColor(getResources().getColor(R.color.black));
                holder.mTextViewPosition.setTextColor(Color.BLACK);
            } else {
                holder.mLinearLayoutItem_bg.setBackgroundColor(getResources().getColor(R.color.white));
                holder.mLinearLayoutItem_bg.setBackgroundColor(Color.GRAY);
            }
            Picasso.with(getApplicationContext()).load(selectedItem.get("user_holder_user_url").toString()).resize(100, 100).transform(new CircleTransform())
                    .centerCrop().placeholder(R.drawable.defult_users)
                    .into(holder.mImageViewUsersPic);

            int result = Classes.getProgamaticalHeights(false, mActivity) - Classes.screen_heights;
            /// holder.LinearLayoutperfectItmeScreen.getLayoutParams().height = result / 7;


            holder.mTextViewPosition.setText(selectedItem.get("user_holder_id").toString());


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ScrollViewPosition.post(new Runnable() {

                        public void run() {
                            Classes.getUserInformationLog();
                            try {
                                ScrollViewPosition.scrollToDeepChild(position + 1);

                                Classes.sendBroadCast(mActivity, "editText_hide_action", String.valueOf(position + 1));
                            } finally {
                                Handler handler = new Handler();
                                Runnable r = new Runnable() {
                                    @Override
                                    public void run() {
                                        updateListViewBackgroundColors();
                                    }
                                };
                                handler.postDelayed(r, 250);
                            }


                        }
                    });
                }
            });
            return convertView;
        }
    }


    public void updateListViewBackgroundColors() {

        String Tag = "updateListViewBackgroundColors";
        ArrayList<Integer> visisble_view_index = new ArrayList<>();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        int globule_reduce_height = Classes.screen_heights + Fragment1.getStatusBarHeight(mActivity);
        int result = height - globule_reduce_height;
        try {
            for (int i = 0; i < Classes.put_user_name.size(); i++) {
                View convertView = findViewById(i + 1);
                Rect scrollBounds = new Rect();
                ScrollViewPosition.getDrawingRect(scrollBounds);
                if (convertView.getLocalVisibleRect(scrollBounds)) {
                    Log.d(Tag + ":  visible view", "yes");
                    if (auto_selected_panal == 2) {
                        if (result / 3 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    } else if (auto_selected_panal == 1) {
                        if (result / 2 == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    } else if (auto_selected_panal == 0) {
                        if (result == scrollBounds.height()) {
                            visisble_view_index.add(i);
                        }
                    }
                } else {
                    Log.d(Tag + ":  visible view", "No");
                }
            }
            gloable_array.clear();
            for (int i = 0; i < Classes.put_user_name.size(); i++) {
                if (visisble_view_index.contains(i)) {
                    gloable_array.add("1");
                } else {
                    gloable_array.add("0");
                }
            }
        } finally {
            adapter.notifyDataSetChanged();
        }
        for (int position = 0; position < Classes.put_user_name.size(); position++) {
            HashMap<String, String> selectedItem = new HashMap<String, String>();
            selectedItem = Classes.getUserInfoByPosition(position);
            String value_user_holder_id = selectedItem.get("user_holder_id").toString();
            String value_user_holder_user_url = selectedItem.get("user_holder_user_url").toString();
            String active_users;
            if (visisble_view_index.contains(position)) {
                gloable_array.add("1");
                active_users = "1";
            } else {
                gloable_array.add("0");
                active_users = "0";
            }
            HashMap<String, String> query_update = new HashMap<String, String>();
            query_update.put("user_holder_background_active", active_users);
            query_update.put("user_holder_id", value_user_holder_id);
            query_update.put("user_holder_user_url", value_user_holder_user_url);
            Classes.updateUserPositionInfomrmation(position, query_update);
        }
        adapter.notifyDataSetChanged();
    }


    public class ViewHolder {
        ImageView mImageViewUsersPic;
        TextView mTextViewPosition;
        LinearLayout mLinearLayoutItem_bg, mLinearLayoutPerfectItemScreen;
    }


    private boolean isChatShowingActive(final View view, final int viewID) {
        Rect scrollBounds = new Rect();
        ScrollViewPosition.getDrawingRect(scrollBounds);
        if (view.getLocalVisibleRect(scrollBounds)) {
            hold_viewID.add(viewID);
            hold_viewHeight.add(scrollBounds.height());
            hold_TempviewHeight.add(scrollBounds.height());
            return true;
        } else {
            return false;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                updateListViewBackgroundColors();
            }
        };
        handler.postDelayed(r, 250);
    }


    class UpdateBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String gloable_heights = intent.getStringExtra("value");
            if (gloable_heights != null) {
                Classes.setMineHandler(1000, new Classes.handlerInterface() {
                    @Override
                    public void run() {
                        try {
                            ScrollViewPosition.scrollToDeepChild(Integer.parseInt(gloable_heights));
                            Classes.setMineHandler(500, new Classes.handlerInterface() {
                                @Override
                                public void run() {
                                    updateView();
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                });
            }
        }
    }

    class ScrollviewUpdate extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String value = intent.getStringExtra("value");
            if (value.equalsIgnoreCase("1")) {
                Classes.addViewDynamic(linearLayoutFragments, mActivity, 88);
            } else {
                Classes.reMoveScrolviedItems(linearLayoutFragments, 88);
            }
        }
    }
}
