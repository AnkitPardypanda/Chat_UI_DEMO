package com.example.root.customelistviews;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by root on 7/12/16.
 */
public class Fragment2 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        Toast.makeText(getActivity(), "2", Toast.LENGTH_SHORT).show();
        return inflater.inflate(R.layout.fragment1,container, false);
    }

}
